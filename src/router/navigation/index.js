export default [{
        title: 'Accueil',
        route: 'apps-home',
    },
    {
        title: 'Présentation',
        route: 'apps-presentation',
    },
    {
        title: 'Catalogues',
        route: 'apps-catalog',
    },
    {
        title: 'Publications',
        route: 'apps-publication',
    },
    {
        title: 'Media',
        route: 'apps-media',
    },
]